import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/pages/home/home.component';
import { NotFoundComponent } from './components/pages/not-found/not-found.component';
import { StudentSearchComponent } from './components/student/student-search/student-search.component';
import { StudentCreateComponent } from './components/student/student-create/student-create.component';
import { ProfessorCreateComponent } from './components/professor/professor-create/professor-create.component';
import { ProfessorSearchComponent } from './components/professor/professor-search/professor-search.component';
import { ProfessorEditComponent } from './components/professor/professor-edit/professor-edit.component';
import { ProfessorTableResolver } from './components/professor/professor-table/professor-table.component.resolver';
import { StudentEditComponent } from './components/student/student-edit/student-edit.component';
import { StudentTableResolver } from './components/student/student-table/student-table.component.resolver';
import { MaterialCategoryCreateComponent } from './components/material-category/material-category-create/material-category-create.component';
import { MaterialCategoryEditComponent } from './components/material-category/material-category-edit/material-category-edit.component';
import { MaterialCategorySearchComponent } from './components/material-category/material-category-search/material-category-search.component';
import { MaterialCategoryTableResolver } from './components/material-category/material-category-table/material-category-table.component.resolver';
import { MaterialFormComponent } from './components/material/material-form/material-form.component';
import { MaterialCreateComponent } from './components/material/material-create/material-create.component';
import { MaterialSearchComponent } from './components/material/material-search/material-search.component';
import { MaterialTableResolver } from './components/material/material-table/material-table.component.resolver';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'student/new',
    component: StudentCreateComponent
  },
  {
    path: 'student/edit/:studentId',
    component: StudentEditComponent
  },
  {
    path: 'student/search',
    component: StudentSearchComponent,
    resolve: {
      resolvedStudentArray: StudentTableResolver
    }
  },
  {
    path: 'professor/new',
    component: ProfessorCreateComponent
  },
  {
    path: 'professor/edit/:professorId',
    component: ProfessorEditComponent
  },
  {
    path: 'professor/search',
    component: ProfessorSearchComponent,
    resolve: {
      resolvedProfessorArray: ProfessorTableResolver
    }
  },
  {
    path: 'material/new',
    component: MaterialCreateComponent
  },
  {
    path: 'material/search',
    component: MaterialSearchComponent,
    resolve: {
      resolvedMaterialArray: MaterialTableResolver
    }
  },
  {
    path: 'category/new',
    component: MaterialCategoryCreateComponent
  },
  {
    path: 'category/edit/:categoryId',
    component: MaterialCategoryEditComponent
  },
  {
    path: 'category/search',
    component: MaterialCategorySearchComponent,
    resolve: {
      resolvedCategoryArray: MaterialCategoryTableResolver
    }
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
