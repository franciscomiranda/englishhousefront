function buildConfig() {

    return {
        version: 'v0.1',
        title: 'English House',
        authors: ['Francisco Miranda', 'Diego Roberto'],
        hostname: 'localhost',
        port: '8080',
        url: this.hostname + ':' + this.port + '/'
    };
}


export const APP_CONFIG = buildConfig();
