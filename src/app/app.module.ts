import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AddressFormComponent } from './components/address/address-form/address-form.component';
import { HomeComponent } from './components/pages/home/home.component';
import { NotFoundComponent } from './components/pages/not-found/not-found.component';
import { StudentFormComponent } from './components/student/student-form/student-form.component';
import { StudentCreateComponent } from './components/student/student-create/student-create.component';
import { StudentEditComponent } from './components/student/student-edit/student-edit.component';

import { MaterialModule } from './modules/material/material.module';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { StudentSearchComponent } from './components/student/student-search/student-search.component';
import { StudentTableComponent } from './components/student/student-table/student-table.component';
import { ProfessorFormComponent } from './components/professor/professor-form/professor-form.component';
import { ProfessorCreateComponent } from './components/professor/professor-create/professor-create.component';
import { ProfessorEditComponent } from './components/professor/professor-edit/professor-edit.component';
import { ProfessorSearchComponent } from './components/professor/professor-search/professor-search.component';
import { ProfessorTableComponent } from './components/professor/professor-table/professor-table.component';
import { ProfessorAddressDialogComponent } from './components/dialogs/professor-address-dialog/professor-address-dialog.component';
import { StudentAddressDialogComponent } from './components/dialogs/student-address-dialog/student-address-dialog.component';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { ProfessorTablePipe } from './components/pipe/professor-table.pipe';
import { StudentTablePipe } from './components/pipe/student-table.pipe';
import { MaterialCategoryFormComponent } from './components/material-category/material-category-form/material-category-form.component';
import { MaterialCategoryCreateComponent } from './components/material-category/material-category-create/material-category-create.component';
import { MaterialCategoryEditComponent } from './components/material-category/material-category-edit/material-category-edit.component';
import { MaterialCategoryTableComponent } from './components/material-category/material-category-table/material-category-table.component';
import { MaterialCategorySearchComponent } from './components/material-category/material-category-search/material-category-search.component';
import { MaterialCategoryFilterPipe } from './components/pipe/material-category-table-filter.pipe';
import { MaterialCategoryDescriptionDialogComponent } from './components/dialogs/material-category-description-dialog/material-category-description-dialog.component';
import { MaterialCreateComponent } from './components/material/material-create/material-create.component';
import { MaterialFormComponent } from './components/material/material-form/material-form.component';
import { MaterialSearchComponent } from './components/material/material-search/material-search.component';
import { MaterialTableComponent } from './components/material/material-table/material-table.component';
import { MaterialTableCategoryCellPipe } from './components/pipe/material-table-category-cell.pipe';

export let options: Partial<IConfig> | (() => Partial<IConfig>) = {};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    StudentFormComponent,
    StudentCreateComponent,
    StudentEditComponent,
    AddressFormComponent,
    ToolbarComponent,
    StudentSearchComponent,
    StudentTableComponent,
    ProfessorFormComponent,
    ProfessorCreateComponent,
    ProfessorEditComponent,
    ProfessorSearchComponent,
    ProfessorTableComponent,
    ProfessorAddressDialogComponent,
    StudentAddressDialogComponent,
    ProfessorTablePipe,
    StudentTablePipe,
    MaterialCategoryFormComponent,
    MaterialCategoryCreateComponent,
    MaterialCategoryEditComponent,
    MaterialCategoryTableComponent,
    MaterialCategorySearchComponent,
    MaterialCategoryFilterPipe,
    MaterialCategoryDescriptionDialogComponent,
    MaterialCreateComponent,
    MaterialFormComponent,
    MaterialSearchComponent,
    MaterialTableComponent,
    MaterialTableCategoryCellPipe
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(options)
  ],
  providers: [],
  entryComponents: [
    ProfessorAddressDialogComponent,
    StudentAddressDialogComponent,
    MaterialCategoryDescriptionDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
