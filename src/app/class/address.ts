export interface Address {
    addressId?: number;
    addressZipCode: string;
    addressStreet: string;
    addressNumber: string;
    addressComplement?: string;
    addressCity?: string;
    addressNeighborhood?: string;
}
