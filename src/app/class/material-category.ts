export interface MaterialCategory {
    materialCategoryId?: number;
    materialCategoryName: string;
    materialCategoryDescription?: string;
    materialCategoryIsActive: boolean;
}
