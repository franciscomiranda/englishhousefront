import { MaterialCategory } from './material-category';
import { Course } from './course';
import { Stock } from './stock';

export interface Material {
    materialId?: number;
    materialName: string;
    materialDescription?: string;
    materialStock?: Stock;
    materialIsForSale: boolean;
    materialSellingPrice?: number;
    materialCategories?: MaterialCategory[];
    materialCourses?: Course[];
    materialIsActive: boolean;
}
