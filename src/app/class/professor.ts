import { Address } from './address';

export interface Professor {
    professorId?: number;
    professorFirstName: string;
    professorLastName?: string;
    professorBirthDate?: Date;
    professorDocumentType: string;
    professorDocumentNumber: string;
    professorPhone?: string;
    professorEmail?: string;
    professorAddress?: Address;
    professorIsActive: boolean;
}
