import { Material } from './material';

export interface Stock {
    stockId?: number;
    stockQuantity?: number;
    stockMinimum?: number;
}
