import { Address } from './address';

export interface Student {
    studentId?: number;
    studentFirstName: string;
    studentLastName?: string;
    studentBirthDate?: Date;
    studentDocumentType: string;
    studentDocumentNumber: string;
    studentPhone?: string;
    studentEmail?: string;
    studentAddress?: Address;
    studentIsActive: boolean;
}
