import { Component, EventEmitter, Input, OnInit, Output, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Address } from 'src/app/class/address';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.scss']
})
export class AddressFormComponent implements OnInit, OnChanges, OnDestroy {

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.addressForm = this.buildAddressForm();
    this.newAddress();
  }

  @Input() addressInput: Address;
  @Output() addressValue = new EventEmitter();
  @Output() addressFormStatusEvent = new EventEmitter();

  address: Address;
  addressForm: FormGroup;
  addressFormSubscription: Subscription;
  subscriptionsArray = [] as Subscription[];


  ngOnChanges({ addressInput: { currentValue } }: SimpleChanges): void {
    if (currentValue) {
      this.setFormValueFromInput(currentValue);
    }
  }

  ngOnInit(): void {
    this.addressOutput();
    this.setSubscriptions();
  }

  ngOnDestroy(): void {
    this.subscriptionsArray
      .forEach(subscription =>
        subscription.unsubscribe());
  }


  newAddress(): void {
    this.address = {} as Address;
  }


  setAddressValue() {
    this.address.addressZipCode = this.addressForm.get('txtAddressZipCode').value;
    this.address.addressStreet = this.addressForm.get('txtAddressStreet').value;
    this.address.addressNumber = this.addressForm.get('txtAddressNumber').value;
    this.address.addressComplement = this.addressForm.get('txtAddressComplement').value;
    this.address.addressCity = this.addressForm.get('txtAddressCity').value;
    this.address.addressNeighborhood = this.addressForm.get('txtAddressNeighborhood').value;
  }

  setFormValueFromInput(currentValue: Address): void {
    this.addressForm.get('txtAddressZipCode').setValue(currentValue.addressZipCode);
    this.addressForm.get('txtAddressStreet').setValue(currentValue.addressStreet);
    this.addressForm.get('txtAddressNumber').setValue(currentValue.addressNumber);
    this.addressForm.get('txtAddressComplement').setValue(currentValue.addressComplement);
    this.addressForm.get('txtAddressCity').setValue(currentValue.addressCity);
    this.addressForm.get('txtAddressNeighborhood').setValue(currentValue.addressNeighborhood);
  }


  buildAddressForm(): FormGroup {
    return this.formBuilder.group({
      txtAddressZipCode: [null, Validators.compose([
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(9)
      ])],
      txtAddressStreet: [null, Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(40)
      ])],
      txtAddressNumber: [null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(5)
      ])
      ],
      txtAddressComplement: [null,
        Validators.maxLength(50)],
      txtAddressCity: [null,
        Validators.maxLength(40)],
      txtAddressNeighborhood: [null,
        Validators.maxLength(30)]
    });
  }

  setSubscriptions(): void {
    let auxSubscription =
      this.addressForm.valueChanges
        .subscribe(() => {
          this.setAddressValue();
          this.addressOutput();
        });
    this.subscriptionsArray
      .push(auxSubscription);

    auxSubscription =
      this.addressForm.statusChanges
        .subscribe((newStatus) =>
          this.statusOutput(newStatus));
    this.subscriptionsArray
      .push(auxSubscription);
  }

  addressOutput(): void {
    this.addressValue.emit(this.address);
  }

  statusOutput(newStatus): void {
    this.addressFormStatusEvent.emit(newStatus);
  }

  resetForm(): void {
    this.addressForm.reset();
  }

}
