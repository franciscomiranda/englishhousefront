import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialCategoryDescriptionDialogComponent } from './material-category-description-dialog.component';

describe('MaterialCategoryDescriptionDialogComponent', () => {
  let component: MaterialCategoryDescriptionDialogComponent;
  let fixture: ComponentFixture<MaterialCategoryDescriptionDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialCategoryDescriptionDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialCategoryDescriptionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
