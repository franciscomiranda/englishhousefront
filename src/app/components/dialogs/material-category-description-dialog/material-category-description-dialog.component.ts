import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { MaterialCategory } from 'src/app/class/material-category';

@Component({
  selector: 'app-material-category-description-dialog',
  templateUrl: './material-category-description-dialog.component.html',
  styleUrls: ['./material-category-description-dialog.component.scss']
})
export class MaterialCategoryDescriptionDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<MaterialCategoryDescriptionDialogComponent>
  ) {
    this.materialCategory = this.data.category;
  }

  materialCategory = {} as MaterialCategory;

  ngOnInit() {
  }

  doClose(): void {
    this.dialogRef.close();
  }
}
