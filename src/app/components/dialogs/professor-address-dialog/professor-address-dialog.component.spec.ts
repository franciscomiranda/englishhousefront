import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessorAddressDialogComponent } from './professor-address-dialog.component';

describe('ProfessorAddressDialogComponent', () => {
  let component: ProfessorAddressDialogComponent;
  let fixture: ComponentFixture<ProfessorAddressDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessorAddressDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessorAddressDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
