import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Professor } from 'src/app/class/professor';

@Component({
  selector: 'app-professor-address-dialog',
  templateUrl: './professor-address-dialog.component.html',
  styleUrls: ['./professor-address-dialog.component.scss']
})
export class ProfessorAddressDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<ProfessorAddressDialogComponent>
  ) {
    this.professor = this.data.professor;
  }

  professor = {
    professorAddress: {}
  } as Professor;

  ngOnInit(): void {
  }

  doClose(): void {
    this.dialogRef.close();
  }

}
