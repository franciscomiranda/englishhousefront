import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentAddressDialogComponent } from './student-address-dialog.component';

describe('StudentAddressDialogComponent', () => {
  let component: StudentAddressDialogComponent;
  let fixture: ComponentFixture<StudentAddressDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentAddressDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentAddressDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
