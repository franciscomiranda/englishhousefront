import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Student } from 'src/app/class/student';

@Component({
  selector: 'app-student-address-dialog',
  templateUrl: './student-address-dialog.component.html',
  styleUrls: ['./student-address-dialog.component.scss']
})
export class StudentAddressDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<StudentAddressDialogComponent>
  ) {
    this.student = this.data.student;
   }

  student = {
    studentAddress: {}
  } as Student;

  ngOnInit(): void {
  }

  doClose(): void {
    this.dialogRef.close();
  }

}
