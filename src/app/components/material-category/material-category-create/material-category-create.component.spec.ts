import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialCategoryCreateComponent } from './material-category-create.component';

describe('MaterialCategoryCreateComponent', () => {
  let component: MaterialCategoryCreateComponent;
  let fixture: ComponentFixture<MaterialCategoryCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialCategoryCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialCategoryCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
