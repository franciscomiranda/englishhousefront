import { Component, OnInit, ViewChild } from '@angular/core';
import { MaterialCategory } from 'src/app/class/material-category';
import Swal, { SweetAlertIcon } from 'sweetalert2';
import { MaterialCategoryService } from 'src/app/service/material-category.service';
import { MaterialCategoryFormComponent } from '../material-category-form/material-category-form.component';

@Component({
  selector: 'app-material-category-create',
  templateUrl: './material-category-create.component.html',
  styleUrls: ['./material-category-create.component.scss']
})
export class MaterialCategoryCreateComponent implements OnInit {

  constructor(
    private materialCategoryService: MaterialCategoryService
  ) {
    this.setNewCategory();
  }

  @ViewChild('materialCategoryComponent', null) materialCategoryComponent: MaterialCategoryFormComponent;

  category: MaterialCategory;
  categoryFormStatus = '';

  ngOnInit() {
  }

  setNewCategory(): void {
    this.category = {} as MaterialCategory;
  }

  setCategoryValueFromEmition(event): void {
    this.category = event;
  }

  setCategoryFormStatusFromEmition(event): void {
    this.categoryFormStatus = event;
  }

  submitCategory(): void {
    if (this.isSubmitValid()) {
      this.materialCategoryService
        .createMaterialCategory(this.category)
        .toPromise()
        .then(() => {
          this.firePopUp('Sucesso!', 'Registro salvo.', 'success');
          this.materialCategoryComponent.categoryForm.reset();
          this.setNewCategory();
        }, onRejected => {
          console.error(onRejected);
          this.firePopUp('Erro', 'Erro ao salvar registro', 'error');
        })
        .catch(error => {
          console.error(error);
          this.firePopUp('Erro', null, 'error');
        });
    } else {
      this.firePopUp(null, 'Verifique os campos', 'warning');
    }
  }

  isSubmitValid(): boolean {
    return this.categoryFormStatus === 'VALID';
  }

  firePopUp(
    givenTitle?: string | HTMLElement | JQuery,
    givenText?: string,
    givenIcon?: SweetAlertIcon
  ): void {
    Swal.fire({
      title: givenTitle,
      text: givenText,
      icon: givenIcon,
      timer: 750,
      backdrop: true,
      position: 'bottom-right'
    });
  }

}
