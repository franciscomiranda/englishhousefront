import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialCategoryEditComponent } from './material-category-edit.component';

describe('MaterialCategoryEditComponent', () => {
  let component: MaterialCategoryEditComponent;
  let fixture: ComponentFixture<MaterialCategoryEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialCategoryEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialCategoryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
