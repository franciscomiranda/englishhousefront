import { Component, OnInit, ViewChild } from '@angular/core';
import { MaterialCategoryFormComponent } from '../material-category-form/material-category-form.component';
import { MaterialCategory } from 'src/app/class/material-category';
import { ActivatedRoute, Router } from '@angular/router';
import { MaterialCategoryService } from 'src/app/service/material-category.service';
import Swal, { SweetAlertIcon } from 'sweetalert2';

@Component({
  selector: 'app-material-category-edit',
  templateUrl: './material-category-edit.component.html',
  styleUrls: ['./material-category-edit.component.scss']
})
export class MaterialCategoryEditComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private materialCategoryService: MaterialCategoryService,
    private router: Router
  ) {
    this.setNewMaterialCategory();
  }

  @ViewChild('materialCategoryFormComponent', null) materialCategoryFormComponent: MaterialCategoryFormComponent;

  materialCategory: MaterialCategory;
  materialCategoryId: number;
  isCategoryActive: boolean;
  materialCategoryFormStatus = '';

  ngOnInit() {
    this.getRouteParameter();
    this.getMaterialCategoryById();
  }

  async getRouteParameter() {
    await this.activatedRoute.params.subscribe(parameters => {
      this.materialCategoryId = parameters.categoryId;
    });
  }

  getMaterialCategoryById() {
    this.materialCategoryService
      .readById(this.materialCategoryId)
      .toPromise()
      .then(materialCategoryResponse => {
        this.materialCategory = materialCategoryResponse;
        this.isCategoryActive = materialCategoryResponse.materialCategoryIsActive;
      }, onRejected => {
        console.error(onRejected);
        this.firePopUp('Erro!', 'Erro ao carregar o registro.', 'error');
      })
      .catch(error => {
        console.error(error);
        this.firePopUp('Erro!', 'Houve um erro.', 'error');
      });
  }

  submitMaterialCategory(): void {
    if (this.isSubmitValid()) {
      this.materialCategory.materialCategoryId = parseInt(this.materialCategoryId.valueOf().toString(), 10);
      this.materialCategory.materialCategoryIsActive = this.isCategoryActive;
      this.materialCategoryService.update(this.materialCategory)
        .toPromise()
        .then(() => {
          this.materialCategoryFormComponent.categoryForm.reset();
          this.firePopUp('Sucesso', null, 'success', () => { this.router.navigate(['/category/search']); });
        }, onRejected => {
          console.error(onRejected);
          this.firePopUp('Erro!', 'Não foi possível salvar.', 'error');
        }).catch(error => {
          console.error(error);
          this.firePopUp('Erro!', 'Houve um erro.', 'error');
        });
    } else {
      this.firePopUp(null, 'Verifique os campos.', 'warning');
    }
  }

  isSubmitValid(): boolean {
    return this.materialCategoryFormStatus === 'VALID';
  }

  setNewMaterialCategory(): void {
    this.materialCategory = {} as MaterialCategory;
  }

  setMaterialCategoryFromEmition(event): void {
    this.materialCategory = event;
  }

  setMaterialCategoryFormStatusFromEmition(event): void {
    this.materialCategoryFormStatus = event;
  }

  firePopUp(
    givenTitle?: string | HTMLElement | JQuery,
    givenText?: string,
    givenIcon?: SweetAlertIcon,
    givenOnDestroy?: any
  ): void {
    Swal.fire({
      position: 'bottom-end',
      title: givenTitle,
      text: givenText,
      icon: givenIcon,
      backdrop: true,
      timer: 1750,
      onDestroy: givenOnDestroy
    });
  }

}
