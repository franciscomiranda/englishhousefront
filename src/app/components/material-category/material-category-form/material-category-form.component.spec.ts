import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialCategoryFormComponent } from './material-category-form.component';

describe('MaterialCategoryFormComponent', () => {
  let component: MaterialCategoryFormComponent;
  let fixture: ComponentFixture<MaterialCategoryFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialCategoryFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialCategoryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
