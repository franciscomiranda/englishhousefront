import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MaterialCategory } from 'src/app/class/material-category';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-material-category-form',
  templateUrl: './material-category-form.component.html',
  styleUrls: ['./material-category-form.component.scss']
})
export class MaterialCategoryFormComponent implements OnInit, OnChanges, OnDestroy {

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.setNewMaterialForm();
    this.setNewMaterialCategory();
    this.setSubscriptions();
  }

  @Input() categoryInput: MaterialCategory;
  @Input() formBehaviorName: string;
  @Output() categoryFormValueEmitter = new EventEmitter();
  @Output() categoryFormStatusEmitter = new EventEmitter();
  @Output() submitEmitter = new EventEmitter();

  category: MaterialCategory;
  categoryForm: FormGroup;
  subscriptionsArray: Subscription[] = [];

  ngOnChanges({ categoryInput: { currentValue } }: SimpleChanges) {
    if (currentValue) {
      this.setMaterialCategoryFormFromInput(currentValue);
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.subscriptionsArray
      .forEach(subscriptions =>
        subscriptions.unsubscribe());
  }

  setNewMaterialCategory() {
    this.category = {} as MaterialCategory;
  }

  setSubscriptions(): void {
    let auxSubscription =
      this.categoryForm.valueChanges
        .subscribe(() => {
          this.setCategoryValue();
          this.emitCategoryFormValue();
        });
    this.subscriptionsArray.push(auxSubscription);

    auxSubscription =
      this.categoryForm.statusChanges
        .subscribe(newStatus => {
          this.emitCategoryFormStatus(newStatus);
        });
    this.subscriptionsArray.push(auxSubscription);
  }

  setCategoryValue(): void {
    this.category.materialCategoryName = this.categoryForm.get('txtCategoryName').value;
    this.category.materialCategoryDescription = this.categoryForm.get('txaCategoryDescription').value;
  }

  setMaterialCategoryFormFromInput(materialCategoryInput): void {
    this.categoryForm.get('txtCategoryName').setValue(materialCategoryInput.materialCategoryName);
    this.categoryForm.get('txaCategoryDescription').setValue(materialCategoryInput.materialCategoryDescription);
  }

  setNewMaterialForm(): void {
    this.categoryForm = this.formBuilder.group({
      txtCategoryName: [null, Validators.compose([
        Validators.required,
        Validators.maxLength(30)
      ])],
      txaCategoryDescription: []
    });
  }

  emitCategoryFormValue(): void {
    this.categoryFormValueEmitter.emit(this.category);
  }

  emitCategoryFormStatus(newStatus): void {
    this.categoryFormStatusEmitter.emit(newStatus);
  }

  emitSubmit(): void {
    this.submitEmitter.emit();
  }

}
