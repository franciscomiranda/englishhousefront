import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialCategorySearchComponent } from './material-category-search.component';

describe('MaterialCategorySearchComponent', () => {
  let component: MaterialCategorySearchComponent;
  let fixture: ComponentFixture<MaterialCategorySearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialCategorySearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialCategorySearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
