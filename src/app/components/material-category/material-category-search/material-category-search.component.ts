import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-material-category-search',
  templateUrl: './material-category-search.component.html',
  styleUrls: ['./material-category-search.component.scss']
})
export class MaterialCategorySearchComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.setNewFilterForm();
  }

  materialCategoryFilterForm: FormGroup;
  nameValue = '';
  statusValue = false;

  ngOnInit() {
  }

  setNameValue(event): void {
    this.nameValue = event.target.value;
  }

  setStatusValue(event): void {
    this.statusValue = event.checked;
  }

  setNewFilterForm(): void {
    this.materialCategoryFilterForm = this.formBuilder.group({
      txtCategoryName: [],
      ckCategoryStatus: []
    });
  }

}
