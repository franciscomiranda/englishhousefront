import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { MaterialCategory } from 'src/app/class/material-category';
import { MaterialCategoryService } from 'src/app/service/material-category.service';

@Injectable({
    providedIn: 'root'
})
export class MaterialCategoryTableResolver implements Resolve<Observable<MaterialCategory[]>> {

    constructor(private materialCategoryService: MaterialCategoryService) { }

    resolve(): Observable<MaterialCategory[]> {
        return this.materialCategoryService.readActives();
    }
}
