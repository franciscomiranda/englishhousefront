import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialCategoryTableComponent } from './material-category-table.component';

describe('MaterialCategoryTableComponent', () => {
  let component: MaterialCategoryTableComponent;
  let fixture: ComponentFixture<MaterialCategoryTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialCategoryTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialCategoryTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
