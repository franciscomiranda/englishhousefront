import { Component, OnInit, Input, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { MaterialCategoryService } from 'src/app/service/material-category.service';
import { MaterialCategory } from 'src/app/class/material-category';
import { Subscription } from 'rxjs';
import Swal, { SweetAlertIcon } from 'sweetalert2';
import { MaterialCategoryDescriptionDialogComponent } from '../../dialogs/material-category-description-dialog/material-category-description-dialog.component';

@Component({
  selector: 'app-material-category-table',
  templateUrl: './material-category-table.component.html',
  styleUrls: ['./material-category-table.component.scss']
})
export class MaterialCategoryTableComponent implements OnInit, OnChanges {

  constructor(
    private activatedRoute: ActivatedRoute,
    private matDialog: MatDialog,
    private materialCategoryService: MaterialCategoryService
  ) { }

  @Input() nameValue: string;
  @Input() statusValue: any;
  @ViewChild(MatPaginator, { static: true }) tablePaginator: MatPaginator;

  categoryTableHasData: boolean;
  categoryTableDataSource: MatTableDataSource<MaterialCategory>;
  subscriptionsArray: Subscription[] = [];
  columnsToDisplay: string[] = [
    'active',
    'name',
    'actions'
  ];

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (propName === 'statusValue') {
        if (changes.statusValue.currentValue === false) {
          this.setCategoryTableDataSourceWithActives();
        } else {
          this.setCategoryTableDataSource();
        }
      }
    }
  }

  ngOnInit() {
    this.setCategoryTableDataSourceFromResolver();
    this.tablePaginator._intl.itemsPerPageLabel = null;
    this.tablePaginator.showFirstLastButtons = true;
  }


  setCategoryTableDataSourceFromResolver(): void {
    const categoryData = this.activatedRoute.snapshot.data.resolvedCategoryArray;
    this.categoryTableDataSource = new MatTableDataSource(categoryData);
    this.categoryTableDataSource.paginator = this.tablePaginator;
    if (categoryData.length > 0) {
      this.categoryTableHasData = true;
    } else {
      this.categoryTableHasData = false;
    }
  }

  setCategoryTableDataSourceWithActives(): void {
    this.materialCategoryService
      .readActives()
      .toPromise()
      .then(materialCategoryData => {
        this.categoryTableDataSource = new MatTableDataSource(materialCategoryData);
        this.categoryTableDataSource.paginator = this.tablePaginator;
        if (materialCategoryData.length > 0) {
          this.categoryTableHasData = true;
        } else {
          this.categoryTableHasData = false;
        }
      }, onRejected => {
        console.error(onRejected);
        this.firePopUp('Erro!', 'Erro ao procurar os registros.', 'error');
      })
      .catch(error => {
        console.error(error);
        this.firePopUp('Erro!', 'Houve um erro.', 'error');
      });
  }

  setCategoryTableDataSource(): void {
    this.materialCategoryService
      .readAll()
      .toPromise()
      .then(materialCategoryData => {
        this.categoryTableDataSource = new MatTableDataSource(materialCategoryData);
        this.categoryTableDataSource.paginator = this.tablePaginator;
        if (materialCategoryData.length > 0) {
          this.categoryTableHasData = true;
        } else {
          this.categoryTableHasData = false;
        }
      }, onRejected => {
        console.error(onRejected);
        this.firePopUp('Erro!', 'Erro ao procurar os registros.', 'error');
      })
      .catch(error => {
        console.error(error);
        this.firePopUp('Erro!', 'Houve um erro.', 'error');
      });
  }


  updateCategoryActiveStatus(category: MaterialCategory): void {
    this.materialCategoryService
      .updateCategoryActiveStatus(category)
      .toPromise()
      .then(() => {
        this.setCategoryTableDataSource();
        this.firePopUp('Sucesso', null, 'success', 750);
      }, onRejected => {
        console.error(onRejected);
        this.firePopUp('Erro!', 'Não foi possível realizar a operação.', 'error');
      }).catch(error => {
        console.error(error);
        this.firePopUp('Erro!', 'Houve um erro.', 'error');
      });
  }

  deleteCategory(category: MaterialCategory): void {
    this.fireAsurePopUp('Confirma?', 'A operação não pode ser desfeita', 'question')
      .then(flag => {
        if (flag) {
          this.materialCategoryService
            .deleteMaterialCategory(category.materialCategoryId)
            .toPromise()
            .then(() => {
              this.setCategoryTableDataSource();
              this.firePopUp('Sucesso', null, 'success');
            }, onRejected => {
              console.error(onRejected);
              this.firePopUp('Erro!', 'Não foi possível excluir o registro.', 'error');
            })
            .catch(error => {
              console.error(error);
              this.firePopUp('Erro!', 'Houve um erro.', 'error');
            });
        }
      });
  }

  openCategoryDescriptionDialog(category: MaterialCategory): void {
    this.matDialog.open(MaterialCategoryDescriptionDialogComponent, {
      data: { category },
      hasBackdrop: true,
      height: '210px',
      width: '570px'
    });
  }

  async fireAsurePopUp(
    givenTitle?: string | HTMLElement | JQuery,
    givenText?: string,
    givenIcon?: SweetAlertIcon,
    givenTimer?: number) {
    const { value: resultFlag } = await Swal.fire({
      title: givenTitle,
      text: givenText,
      icon: givenIcon,
      backdrop: true,
      position: 'bottom',
      confirmButtonText: 'Sim',
      confirmButtonColor: 'primary',
      showConfirmButton: true,
      cancelButtonText: 'Não',
      cancelButtonColor: 'warn',
      showCancelButton: true,
      reverseButtons: true
    });
    return resultFlag;
  }

  firePopUp(
    givenTitle?: string | HTMLElement | JQuery,
    givenText?: string,
    givenIcon?: SweetAlertIcon,
    givenTimer?: number) {
    Swal.fire({
      title: givenTitle,
      text: givenText,
      icon: givenIcon,
      timer: givenTimer ? givenTimer : 1750,
      backdrop: true,
      position: 'bottom-end'
    });
  }
}
