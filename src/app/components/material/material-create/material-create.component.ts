import { Component, OnInit, ViewChild } from '@angular/core';
import { Material } from 'src/app/class/material';
import { MaterialService } from 'src/app/service/material.service';
import Swal, { SweetAlertIcon } from 'sweetalert2';
import { MaterialFormComponent } from '../material-form/material-form.component';

@Component({
  selector: 'app-material-create',
  templateUrl: './material-create.component.html',
  styleUrls: ['./material-create.component.scss']
})
export class MaterialCreateComponent implements OnInit {

  constructor(private materialService: MaterialService) {
    this.setNewMaterial();
  }

  @ViewChild('materialComponent', null) materialComponent: MaterialFormComponent;

  material: Material;
  materialFormStatus = '';

  ngOnInit(): void {
  }

  isSubmitValid(): boolean {
    return this.materialFormStatus === 'VALID';
  }

  setNewMaterial(): void {
    this.material = {} as Material;
  }

  setMaterialValueFromEmition(event): void {
    this.material = event;
  }

  setMaterialFormStatusFromEmition(event): void {
    this.materialFormStatus = event;
  }

  submitMaterial(): void {
    console.log(this.material);
    if (this.isSubmitValid()) {
      this.materialService
        .postMaterial(this.material)
        .toPromise()
        .then(() => {
          this.firePopUp('Sucesso!', 'Registro salvo.', 'success');
          this.materialComponent.materialForm.reset();
          this.setNewMaterial();
        }, onRejected => {
          console.error(onRejected);
          this.firePopUp('Erro', 'Erro ao salvar registro', 'error');
        }).catch(error => {
          console.error(error);
          this.firePopUp('Erro', null, 'error');
        });
    }
  }

  firePopUp(
    givenTitle?: string | HTMLElement | JQuery,
    givenText?: string,
    givenIcon?: SweetAlertIcon
  ): void {
    Swal.fire({
      title: givenTitle,
      text: givenText,
      icon: givenIcon,
      timer: 750,
      backdrop: true,
      position: 'bottom-right'
    });
  }
}
