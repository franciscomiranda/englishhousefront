import { Component, OnInit, Input, ViewChild, OnDestroy, ElementRef, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MaterialCategoryService } from 'src/app/service/material-category.service';
import { MaterialCategory } from 'src/app/class/material-category';
import { MatCheckbox, MatSelectionList, MatSelectionListChange } from '@angular/material';
import { Material } from 'src/app/class/material';
import { Subscription } from 'rxjs';
import { Stock } from 'src/app/class/stock';

@Component({
  selector: 'app-material-form',
  templateUrl: './material-form.component.html',
  styleUrls: ['./material-form.component.scss']
})
export class MaterialFormComponent implements OnInit, OnDestroy {

  constructor(
    private formBuilder: FormBuilder,
    private materialCategoryService: MaterialCategoryService
  ) {
    this.setCategoryArray();
    this.setMaterialForm();
    this.setNewMaterial();
    this.setSubscriptions();
  }

  @Input() formBehaviorName: string;
  @Output() materialFormValueEmitter = new EventEmitter();
  @Output() materialFormStatusEmitter = new EventEmitter();
  @Output() submitEmitter = new EventEmitter();

  categoryArray: MaterialCategory[] = [];
  material: Material;
  materialForm: FormGroup;
  selectedCategoryOptions: any[] = [];
  subscriptionArray: Subscription[] = [];

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subscriptionArray
      .forEach(subscription =>
        subscription
          .unsubscribe());
  }

  emitMaterialFormValue(): void {
    this.materialFormValueEmitter.emit(this.material);
  }

  emitMaterialFormStatus(newStatus): void {
    this.materialFormStatusEmitter.emit(newStatus);
  }

  emitSubmit(): void {
    this.submitEmitter.emit();
  }

  getSelectedCategories(): MaterialCategory[] {
    const selectedCategories: MaterialCategory[] = [];
    this.selectedCategoryOptions.forEach(option => selectedCategories.push(option.value));
    return selectedCategories;
  }

  resetCheckBoxes(){

  }

  setCategoryArray(): void {
    this.materialCategoryService
      .readAll()
      .toPromise()
      .then(categoryResponse => {
        this.categoryArray =
          categoryResponse
            .filter(category =>
              category.materialCategoryIsActive);
      }, responseError => {
        console.error(responseError);
      })
      .catch(error => console.error(error)
      );
  }

  setMaterialValue(): void {
    this.material.materialName = this.materialForm.get('txtMaterialName').value;
    this.material.materialDescription = this.materialForm.get('txtMaterialDescription').value;
    if (!this.material.materialId) { this.material.materialStock.stockQuantity = this.materialForm.get('txtMaterialStartingStock').value; }
    this.material.materialStock.stockMinimum = this.materialForm.get('txtMaterialMinimumStock').value;
    this.material.materialSellingPrice = this.materialForm.get('txtMaterialSellingPrice').value;
    this.material.materialCategories = this.getSelectedCategories();
  }

  setNewMaterial(): void {
    this.material = {
      materialStock: {}
    } as Material;
  }

  setSelectedCategories(event: MatSelectionListChange): void {
    this.selectedCategoryOptions = [];
    this.selectedCategoryOptions = event.source.options
      .filter(option => option.selected)
      .filter(option => option.value);
  }

  setSubscriptions(): void {
    let auxSubscription =
      this.materialForm
        .valueChanges
        .subscribe(newV => {
          this.setMaterialValue();
          this.emitMaterialFormValue();
          console.log(newV.cbMinimumStock);
          
          
        });

    this.subscriptionArray.push(auxSubscription);

    auxSubscription =
      this.materialForm
        .statusChanges
        .subscribe(newStatus => {
          this.emitMaterialFormStatus(newStatus);
        });

    this.subscriptionArray.push(auxSubscription);
  }

  setMaterialForm(): void {
    this.materialForm = this.formBuilder.group({
      txtMaterialName: [null, Validators.compose([
        Validators.required,
        Validators.maxLength(30)
      ])],
      txtMaterialDescription: [null, Validators.maxLength(300)],
      cbStartingStock: [false],
      txtMaterialStartingStock: [],
      cbMinimumStock: [false],
      txtMaterialMinimumStock: [],
      cbSaleAvailability: [false],
      txtMaterialSellingPrice: [],
    });
    this.materialForm.controls.txtMaterialStartingStock.disable();
    this.materialForm.controls.txtMaterialMinimumStock.disable();
    this.materialForm.controls.txtMaterialSellingPrice.disable();
  }

  toggleStartingStock(event): void {
    event.checked ?
      this.materialForm.controls.txtMaterialStartingStock.enable() :
      this.materialForm.controls.txtMaterialStartingStock.disable();
    this.materialForm.controls.txtMaterialStartingStock.reset();
  }

  toggleMinimumStock(event): void {
    event.checked ?
      this.materialForm.controls.txtMaterialMinimumStock.enable() :
      this.materialForm.controls.txtMaterialMinimumStock.disable();
    this.materialForm.controls.txtMaterialMinimumStock.reset();
  }

  toggleSaleAvailability(event): void {
    event.checked ?
      (this.materialForm.controls.txtMaterialSellingPrice.enable(), this.material.materialIsForSale = true) :
      (this.materialForm.controls.txtMaterialSellingPrice.disable(), this.material.materialIsForSale = false);
    this.materialForm.controls.txtMaterialSellingPrice.reset();
  }

}
