import { Injectable } from "@angular/core";
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { Material } from 'src/app/class/material';
import { MaterialService } from 'src/app/service/material.service';

@Injectable({
    providedIn: 'root'
})
export class MaterialTableResolver implements Resolve<Observable<Material[]>>{

    constructor(private materialService: MaterialService) { }

    resolve(): Observable<Material[]> {
        return this.materialService.getActiveMaterial();
    }
}
