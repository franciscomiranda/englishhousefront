import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Material } from 'src/app/class/material';
import { MaterialService } from 'src/app/service/material.service';
import Swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-material-table',
  templateUrl: './material-table.component.html',
  styleUrls: ['./material-table.component.scss']
})
export class MaterialTableComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private materialService: MaterialService) {

  }

  @ViewChild(MatPaginator, { static: true }) tablePaginator: MatPaginator;

  materialTableDataSource: MatTableDataSource<Material>;
  materialTableHasData: boolean;

  columnsToDisplay = [
    'active',
    'name',
    'category',
    'stock',
    'price',
    'actions'
  ];

  ngOnInit(): void {
    this.setMaterialDataTableFromResolver();
    this.tablePaginator._intl.itemsPerPageLabel = null;
    this.tablePaginator.showFirstLastButtons = true;
  }

  setMaterialTableDataSource(): void {
    this.materialService
      .getAllMaterial()
      .toPromise()
      .then(resolvedData => {
        this.materialTableDataSource = new MatTableDataSource(resolvedData);
        this.materialTableDataSource.paginator = this.tablePaginator;
        if (resolvedData.length > 0) {
          this.materialTableHasData = true;
        } else {
          this.materialTableHasData = false;
        }
      }, onRejected => {
        console.error(onRejected);

      });
  }

  setMaterialDataTableFromResolver(): void {
    const materialData = this.activatedRoute.snapshot.data.resolvedMaterialArray;
    this.materialTableDataSource = new MatTableDataSource(materialData);
    this.materialTableDataSource.paginator = this.tablePaginator;
    if (materialData.length > 0) {
      this.materialTableHasData = true;
    } else {
      this.materialTableHasData = false;
    }
  }

  setMaterialDataTableWithActives(): void {
    this.materialService
      .getActiveMaterial()
      .toPromise()
      .then(materialData => {
        this.materialTableDataSource = new MatTableDataSource(materialData);
        this.materialTableDataSource.paginator = this.tablePaginator;
        if (materialData.length > 0) {
          this.materialTableHasData = true;
        } else {
          this.materialTableHasData = false;
        }
      }, onRejected => {
        console.error(onRejected);

      })
      .catch(error => {
        console.error(error);

      });
  }


}
