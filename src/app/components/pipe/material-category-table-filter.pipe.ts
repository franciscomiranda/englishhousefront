import { Pipe, PipeTransform } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { MaterialCategory } from 'src/app/class/material-category';

@Pipe({
    name: 'materialCategoryFilter'
})
export class MaterialCategoryFilterPipe implements PipeTransform {

    transform(dataSource: any, ...args: any[]): any {
        const nameValue = args[0].trim().toLowerCase();
        const statusValue = args[1];
        const tablePaginator = args[2];
        let finalDataSource: MatTableDataSource<MaterialCategory>;
        dataSource = dataSource._data._value;
        if (nameValue) {
            const data = dataSource
                .filter(category =>
                    category.materialCategoryName
                        .toLowerCase()
                        .includes(nameValue));
            finalDataSource = new MatTableDataSource(data);
            finalDataSource.paginator = tablePaginator;
            return finalDataSource;
        } else {
            finalDataSource = new MatTableDataSource(dataSource);
            finalDataSource.paginator = tablePaginator;
            return finalDataSource;
        }
    }
}
