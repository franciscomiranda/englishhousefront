import { Pipe, PipeTransform } from "@angular/core";
import { MaterialCategory } from 'src/app/class/material-category';
import { Material } from 'src/app/class/material';

@Pipe({
    name: 'materialTableCategoryCellPipe'
})
export class MaterialTableCategoryCellPipe implements PipeTransform {
    transform(material: Material): string {
        let finalCategoryString = '';
        material.materialCategories.forEach(element => {
            finalCategoryString += element.materialCategoryName.toString() + ' ';
        });
        return finalCategoryString;
    }
}
