import { Pipe, PipeTransform } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Professor } from 'src/app/class/professor';

@Pipe({
  name: 'professorTable'
})
export class ProfessorTablePipe implements PipeTransform {

  transform(dataSource: any, ...searchValue: any[]): any {
    const nameValue = searchValue[0].trim().toLowerCase();
    const documentValue = searchValue[1].trim().toLowerCase();
    const statusValue = searchValue[2];
    const tablePaginator = searchValue[3];
    let finalDataSource: MatTableDataSource<Professor>;
    dataSource = dataSource._data._value;
    if (nameValue || documentValue || !statusValue) {
      const data = dataSource
        .filter(professor =>
          this.getProfessorFullName(professor)
            .toLowerCase()
            .includes(nameValue))
        .filter(professor =>
          professor.professorDocumentNumber
            .includes(documentValue))
        .filter(professor =>
          professor.professorIsActive === !statusValue);
      finalDataSource = new MatTableDataSource(data);
      finalDataSource.paginator = tablePaginator;
      return finalDataSource;
    } else {
      finalDataSource = new MatTableDataSource(dataSource);
      finalDataSource.paginator = tablePaginator;
      return finalDataSource;
    }
  }

  getProfessorFullName(professor: Professor): string {
    return professor.professorLastName ?
      professor.professorFirstName + ' ' + professor.professorLastName :
      professor.professorFirstName;
  }

}
