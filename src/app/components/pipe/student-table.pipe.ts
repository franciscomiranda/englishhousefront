import { MatTableDataSource } from '@angular/material';
import { Pipe, PipeTransform } from '@angular/core';
import { Student } from 'src/app/class/student';

@Pipe({
  name: 'studentTable'
})
export class StudentTablePipe implements PipeTransform {

  transform(dataSource: any, ...searchValue: any[]): any {
    const nameValue = searchValue[0].trim().toLowerCase();
    const documentValue = searchValue[1].trim().toLowerCase();
    const statusValue = searchValue[2];
    const tablePaginator = searchValue[3];
    let finalDataSource: MatTableDataSource<Student>;
    dataSource = dataSource._data._value;
    if (nameValue || documentValue || !statusValue) {
      const data = dataSource
        .filter(student =>
          this.getStudentFullName(student)
            .toLowerCase()
            .includes(nameValue))
        .filter(student =>
            student.studentDocumentNumber
            .includes(documentValue))
        .filter(student =>
            student.studentIsActive === !statusValue);
      finalDataSource = new MatTableDataSource(data);
      finalDataSource.paginator = tablePaginator;
      return finalDataSource;
    } else {
      finalDataSource = new MatTableDataSource(dataSource);
      finalDataSource.paginator = tablePaginator;
      return finalDataSource;
    }
  }

  getStudentFullName(student: Student): string {
    return student.studentLastName ?
      student.studentFirstName + ' ' + student.studentLastName :
      student.studentFirstName;
  }

}
