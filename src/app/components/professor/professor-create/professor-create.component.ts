import { Component, OnInit, ViewChild } from '@angular/core';
import { Professor } from 'src/app/class/professor';
import { ProfessorFormComponent } from '../professor-form/professor-form.component';
import { ProfessorService } from 'src/app/service/professor.service';
import Swal, { SweetAlertIcon } from 'sweetalert2';

@Component({
  selector: 'app-professor-create',
  templateUrl: './professor-create.component.html',
  styleUrls: ['./professor-create.component.scss']
})
export class ProfessorCreateComponent implements OnInit {

  constructor(
    private professorService: ProfessorService
  ) {
    this.newProfessor();
  }

  @ViewChild('professorComponent', null) professorComponent: ProfessorFormComponent;

  professor: Professor;
  professorFormStatus = '';
  addressFormStatus = '';

  ngOnInit(): void {
  }


  newProfessor(): void {
    this.professor = {
      professorAddress: {}
    } as Professor;
  }


  submitProfessor(): void {
    if (this.isSubmitValid()) {
      this.professorService
        .createProfessor(this.professor)
        .toPromise()
        .then(() => {
          this.professorComponent.professorForm.reset();
          this.professorComponent.resetChildForm();
          this.firePopUp('Sucesso!', 'Salvo com sucesso.', 'success');
          this.newProfessor();
        }, onRejected => {
          console.error(onRejected);
          this.firePopUp('Erro!', 'Não foi possível salvar.', 'error');
        })
        .catch(error => {
          console.error(error);
          this.firePopUp('Erro!', 'Houve um erro.', 'error');
        });
    } else {
      this.firePopUp('', 'Verifique os campos.', 'warning');
    }
  }


  isSubmitValid(): boolean {
    return this.professorFormStatus === 'VALID' && this.addressFormStatus === 'VALID';
  }


  setProfessorFormValueFromEmition(event): void {
    this.professor = event;
  }

  setProfessorFormStatusFromEmition(event): void {
    this.professorFormStatus = event;
  }

  setProfessorAddressValueFromEmition(event): void {
    this.professor.professorAddress = event;
  }

  setAddressFormStatusFromEmition(event): void {
    this.addressFormStatus = event;
  }


  firePopUp(
    givenTitle?: string | HTMLElement | JQuery,
    givenText?: string,
    givenIcon?: SweetAlertIcon
  ): void {
    Swal.fire({
      position: 'bottom-end',
      title: givenTitle,
      text: givenText,
      icon: givenIcon,
      backdrop: true,
      timer: 1750
    });
  }

}
