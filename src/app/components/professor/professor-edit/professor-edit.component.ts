import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfessorService } from 'src/app/service/professor.service';
import { Professor } from 'src/app/class/professor';
import Swal, { SweetAlertIcon } from 'sweetalert2';
import { ProfessorFormComponent } from '../professor-form/professor-form.component';

@Component({
  selector: 'app-professor-edit',
  templateUrl: './professor-edit.component.html',
  styleUrls: ['./professor-edit.component.scss']
})
export class ProfessorEditComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private professorService: ProfessorService,
    private router: Router
  ) {
    this.newProfessor();
  }

  @ViewChild('professorComponent', null) professorComponent: ProfessorFormComponent;

  professor: Professor;
  professorId: number;
  addressId: number;
  isProfessorActive: boolean;
  professorFormStatus = '';
  addressFormStatus = '';

  async ngOnInit() {
    await this.activatedRoute.params
      .subscribe(parameters => {
        this.professorId = parameters.professorId;
      });

    this.professorService
      .readProfessorById(this.professorId)
      .toPromise()
      .then(professorResponse => {
        this.professor = professorResponse;
        this.addressId = professorResponse.professorAddress.addressId;
        this.isProfessorActive = professorResponse.professorIsActive;
      }, responseError => {
        console.error(responseError);
        this.firePopUp('Erro!', 'Erro ao carregar Professor.', 'error');
      }).catch(error => {
        console.error(error);
        this.firePopUp('Erro!', 'Houve um erro.', 'error');
      });
  }

  newProfessor(): void {
    this.professor = {
      professorAddress: {}
    } as Professor;
  }

  submitProfessor() {
    if (this.isSubmitValid()) {
      this.professor.professorId = parseInt(this.professorId.valueOf().toString(), 10);
      this.professor.professorAddress.addressId = parseInt(this.addressId.valueOf().toString(), 10);
      this.professor.professorIsActive = this.isProfessorActive;
      this.professorService.updateProfessor(this.professor)
        .toPromise()
        .then(() => {
          this.professorComponent.professorForm.reset();
          this.professorComponent.resetChildForm();
          this.firePopUp('Sucesso', null, 'success', () => { this.router.navigate(['/professor/search']); });
        }, responseError => {
          console.error(responseError);
          this.firePopUp('Erro!', 'Não foi possível salvar.', 'error');
        })
        .catch(error => {
          console.error(error);
          this.firePopUp('Erro!', 'Houve um erro.', 'error');
        });
    } else {
      this.firePopUp(null, 'Verifique os campos.', 'warning');
    }
  }

  isSubmitValid(): boolean {
    return this.professorFormStatus === 'VALID' && this.addressFormStatus === 'VALID';
  }

  setProfessorValueFromEmition(event): void {
    this.professor = event;
  }

  setProfessorFormStatusFromEmition(event): void {
    this.professorFormStatus = event;
  }

  setProfessorAddressValueFromEmition(event): void {
    this.professor.professorAddress = event;
  }

  setAddressFormStatusFromEmition(event): void {
    this.addressFormStatus = event;
  }


  firePopUp(
    givenTitle?: string | HTMLElement | JQuery,
    givenText?: string,
    givenIcon?: SweetAlertIcon,
    givenOnDestroy?: any
  ): void {
    Swal.fire({
      position: 'bottom-end',
      title: givenTitle,
      text: givenText,
      icon: givenIcon,
      backdrop: true,
      timer: 1750,
      onDestroy: givenOnDestroy
    });
  }
}
