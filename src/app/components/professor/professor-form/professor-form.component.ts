import { Component, EventEmitter, OnInit, Input, Output, OnChanges, OnDestroy, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DocumentType } from 'src/app/class/document-type';
import { Professor } from 'src/app/class/professor';
import { Subscription } from 'rxjs';
import { AddressFormComponent } from '../../address/address-form/address-form.component';
import { Address } from 'src/app/class/address';

@Component({
  selector: 'app-professor-form',
  templateUrl: './professor-form.component.html',
  styleUrls: ['./professor-form.component.scss']
})
export class ProfessorFormComponent implements OnInit, OnChanges, OnDestroy {

  constructor(
    private formBuilder: FormBuilder,
  ) {
    this.buildForm();
    this.setDocumentTypeArray();
    this.setSubscriptions();
    this.newProfessor();
  }

  @Input() professorInput: Professor;
  @Input() formBehaviorName: string;
  @Output() professorEmitter = new EventEmitter();
  @Output() professorFormStatusEmitter = new EventEmitter();
  @Output() addressEmitter = new EventEmitter();
  @Output() addressFormStatusEmitter = new EventEmitter();
  @Output() submitEmitter = new EventEmitter();
  @ViewChild('addressComponent', null) addressComponent: AddressFormComponent;
  @ViewChild('documentNumber', null) documentNumber: ElementRef;
  @ViewChild('documentType', null) documentType: any;
  @ViewChild('phone', null) phone: any;

  professor: Professor;
  professorForm: FormGroup;
  professorAddress: Address;
  documentTypeArray = [] as DocumentType[];
  subscriptionArray = [] as Subscription[];
  documentMask: string;
  phoneMask: string;

  ngOnChanges({ professorInput: { currentValue } }: SimpleChanges) {
    if (currentValue) {
      this.setProfessorFormValueFromInput(currentValue);
      this.professorAddress = currentValue.professorAddress;
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subscriptionArray
      .forEach(subscription =>
        subscription.unsubscribe());
  }

  newProfessor(): void {
    this.professor = {} as Professor;
  }

  setProfessorValue(): void {
    this.professor.professorFirstName = this.professorForm.get('txtProfessorFirstName').value;
    this.professor.professorLastName = this.professorForm.get('txtProfessorLastName').value;
    this.professor.professorBirthDate = this.professorForm.get('dtProfessorBirthDate').value;
    this.professor.professorDocumentType = this.professorForm.get('slProfessorDocumentType').value;
    this.professor.professorDocumentNumber = this.professorForm.get('txtProfessorDocumentNumber').value;
    this.professor.professorPhone = this.professorForm.get('txtProfessorPhone').value;
    this.professor.professorEmail = this.professorForm.get('txtProfessorEmail').value;
  }

  setProfessorFormValueFromInput(professorInput): void {
    this.professorForm.get('txtProfessorFirstName').setValue(professorInput.professorFirstName);
    this.professorForm.get('txtProfessorLastName').setValue(professorInput.professorLastName);
    this.professorForm.get('dtProfessorBirthDate').setValue(professorInput.professorBirthDate);
    this.professorForm.get('slProfessorDocumentType').setValue(professorInput.professorDocumentType);
    this.professorForm.get('txtProfessorDocumentNumber').setValue(professorInput.professorDocumentNumber);
    this.professorForm.get('txtProfessorPhone').setValue(professorInput.professorPhone);
    this.professorForm.get('txtProfessorEmail').setValue(professorInput.professorEmail);
  }

  setSubscriptions(): void {
    let auxSubscription =
      this.professorForm.valueChanges
        .subscribe(() => {
          this.setProfessorValue();
          this.emitProfessor();
        });
    this.subscriptionArray
      .push(auxSubscription);

    auxSubscription =
      this.professorForm.statusChanges
        .subscribe(newProfessorFormStatus =>
          this.emitProfessorFormStatus(newProfessorFormStatus));
    this.subscriptionArray
      .push(auxSubscription);
  }

  setDocumentMaskFormat(): void {
    const stringType: string = this.documentType.value;
    if (stringType === 'CPF') {
      this.documentMask = '000.000.000-00';
    } else if (stringType === 'CNPJ') {
      this.documentMask = '00.000.000/0000-00';
    }
  }

  setPhoneMaskFormat(): void {
    const stringValue: string = this.phone.nativeElement.value;
    const stringLength = stringValue.length;
    if (stringLength === 13) {
      this.phoneMask = '(00)0000-00000';
    } else if (stringLength === 14) {
      this.phoneMask = '(00)00000-0000';
    }
  }

  setDocumentTypeArray(): void {
    this.documentTypeArray = [
      { documentTypeId: 1, documentTypeName: 'CPF' },
      { documentTypeId: 2, documentTypeName: 'CNPJ' }
    ];
  }

  emitProfessor(): void {
    this.professorEmitter.emit(this.professor);
  }

  emitProfessorFormStatus(newStatus): void {
    this.professorFormStatusEmitter.emit(newStatus);
  }

  emitAddress(event): void {
    this.addressEmitter.emit(event);
  }

  emitAddressFormStatus(event): void {
    this.addressFormStatusEmitter.emit(event);
  }

  emitSubmit(): void {
    this.submitEmitter.emit();
  }


  resetChildForm() {
    this.addressComponent.resetForm();
  }

  buildForm(): void {
    this.professorForm = this.formBuilder.group({
      txtProfessorFirstName: [null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(25)
      ])],
      txtProfessorLastName: [null, Validators.maxLength(40)],
      dtProfessorBirthDate: [null],
      slProfessorDocumentType: [null, Validators.required],
      txtProfessorDocumentNumber: [null, Validators.compose([
        Validators.required,
        Validators.minLength(11),
        Validators.maxLength(14)
      ])],
      txtProfessorPhone: [null, Validators.compose([
        Validators.minLength(10),
        Validators.maxLength(11)
      ])],
      txtProfessorEmail: [null, Validators.compose([
        Validators.email,
        Validators.maxLength(50)
      ])]
    });
    this.professorForm.get('dtProfessorBirthDate').disable();
  }
}
