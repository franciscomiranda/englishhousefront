import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ProfessorTableComponent } from '../professor-table/professor-table.component';

@Component({
  selector: 'app-professor-search',
  templateUrl: './professor-search.component.html',
  styleUrls: ['./professor-search.component.scss']
})
export class ProfessorSearchComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.professorSearchForm = this.getSearchForm();
  }

  professorSearchForm: FormGroup;
  nameValue = '';
  documentValue = '';
  statusValue = false;

  ngOnInit() {
  }

  setNameValue(value): void {
    this.nameValue = value.target.value;
  }
  setDocumentValue(value): void {
    this.documentValue = value.target.value;
  }
  setStatusValue(value): void {
    this.statusValue = value.checked;
  }

  getSearchForm(): FormGroup {
    return this.formBuilder.group({
      txtProfessorName: [],
      txtProfessorDocument: [],
      ckProfessorStatus: []
    });
  }
}
