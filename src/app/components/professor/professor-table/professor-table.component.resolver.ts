import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { Professor } from 'src/app/class/professor';
import { ProfessorService } from 'src/app/service/professor.service';

@Injectable({
    providedIn: 'root'
})
export class ProfessorTableResolver implements Resolve<Observable<Professor[]>> {

    constructor(private professorService: ProfessorService) { }

    resolve(): Observable<Professor[]> {
        return this.professorService.readAll();
    }
}
