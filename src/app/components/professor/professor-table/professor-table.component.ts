import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { Professor } from 'src/app/class/professor';
import { ProfessorAddressDialogComponent } from '../../dialogs/professor-address-dialog/professor-address-dialog.component';
import { ProfessorService } from 'src/app/service/professor.service';
import { Subscription } from 'rxjs';
import Swal, { SweetAlertIcon } from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-professor-table',
  templateUrl: './professor-table.component.html',
  styleUrls: ['./professor-table.component.scss']
})
export class ProfessorTableComponent implements OnInit, OnDestroy {

  constructor(
    private activatedRoute: ActivatedRoute,
    private matDialog: MatDialog,
    private professorService: ProfessorService
  ) { }

  @Input() nameValue: string;
  @Input() documentValue: string;
  @Input() statusValue: any;
  @ViewChild(MatPaginator, { static: true }) tablePaginator: MatPaginator;

  professorTableHasData: boolean;
  professorTableDataSource: MatTableDataSource<Professor>;
  subscriptionsArray: Subscription[] = [];
  columnsToDisplay: string[] = [
    'active',
    'name',
    'birth',
    'documentType',
    'documentNumber',
    'phone',
    'email',
    'actions'
  ];

  ngOnInit() {
    this.setProfessorTableDataSourceFromResolver();
    this.tablePaginator._intl.itemsPerPageLabel = null;
    this.tablePaginator.showFirstLastButtons = true;
  }

  ngOnDestroy() {
    this.subscriptionsArray
      .forEach(subscription =>
        subscription.unsubscribe());
  }


  setProfessorTableDataSource(): void {
    this.professorService
      .readAll()
      .toPromise()
      .then(professorData => {
        this.professorTableDataSource = new MatTableDataSource(professorData);
        this.professorTableDataSource.paginator = this.tablePaginator;
        if (professorData.length > 0) {
          this.professorTableHasData = true;
        } else {
          this.professorTableHasData = false;
        }
      }, responseError => {
        console.error(responseError);
        this.firePopUp('Erro!', 'Erro ao procurar os registros.', 'error');
      }).catch(error => {
        console.error(error);
        this.firePopUp('Erro!', 'Houve um erro.', 'error');
      });
  }

  setProfessorTableDataSourceFromResolver(): void {
    const professorData = this.activatedRoute.snapshot.data.resolvedProfessorArray;
    this.professorTableDataSource = new MatTableDataSource(professorData);
    this.professorTableDataSource.paginator = this.tablePaginator;
    if (professorData.length > 0) {
      this.professorTableHasData = true;
    } else {
      this.professorTableHasData = false;
    }
  }

  getProfessorFullName(professor: Professor): string {
    return professor.professorLastName ?
      professor.professorFirstName + ' ' + professor.professorLastName :
      professor.professorFirstName;
  }

  deleteProfessor(professor: Professor) {
    this.fireAsurePopUp('Confirma?', 'A operação não pode ser desfeita.', 'question')
      .then(flag => {
        if (flag) {
          this.professorService
            .deleteProfessor(professor.professorId)
            .toPromise()
            .then(() => {
              this.setProfessorTableDataSource();
              this.firePopUp('Sucesso!', null, 'success');
            }, onRejected => {
              console.error(onRejected);
              this.firePopUp('Erro!', 'Não foi possível excluir o registro.', 'error');
            })
            .catch(error => {
              console.error(error);
              this.firePopUp('Erro!', 'Houve um erro.', 'error');
            });
        }
      });
  }

  toggleProfessorActiveStatus(professor: Professor) {
    this.professorService
      .updateProfessorActiveStatus(professor)
      .toPromise()
      .then(() => {
        this.setProfessorTableDataSource();
        this.firePopUp('Sucesso', null, 'success', 750);
      }, responseError => {
        console.error(responseError);
        this.firePopUp('Erro!', 'Não foi possível realizar a operação.', 'error');
      }).catch(error => {
        console.error(error);
        this.firePopUp('Erro!', 'Houve um erro.', 'error');
      });
  }

  openProfessorAddressDialog(professor: Professor): void {
    this.matDialog.open(ProfessorAddressDialogComponent, {
      data: { professor },
      hasBackdrop: true,
      height: '227px',
      width: '670px'
    });
  }

  async fireAsurePopUp(
    givenTitle?: string | HTMLElement | JQuery,
    givenText?: string,
    givenIcon?: SweetAlertIcon,
    givenTimer?: number) {
    const { value: resultFlag } = await Swal.fire({
      title: givenTitle,
      text: givenText,
      icon: givenIcon,
      backdrop: true,
      position: 'bottom',
      confirmButtonText: 'Sim',
      confirmButtonColor: 'primary',
      showConfirmButton: true,
      cancelButtonText: 'Não',
      cancelButtonColor: 'warn',
      showCancelButton: true,
      reverseButtons: true
    });
    return resultFlag;
  }

  firePopUp(
    givenTitle?: string | HTMLElement | JQuery,
    givenText?: string,
    givenIcon?: SweetAlertIcon,
    givenTimer?: number) {
    Swal.fire({
      title: givenTitle,
      text: givenText,
      icon: givenIcon,
      timer: givenTimer ? givenTimer : 1750,
      backdrop: true,
      position: 'bottom-end'
    });
  }

}
