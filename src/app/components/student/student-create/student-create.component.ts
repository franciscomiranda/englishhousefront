import { Component, OnInit, ViewChild } from '@angular/core';
import { StudentService } from 'src/app/service/student.service';
import { Student } from 'src/app/class/student';
import { StudentFormComponent } from '../student-form/student-form.component';
import Swal, { SweetAlertIcon } from 'sweetalert2';

@Component({
  selector: 'app-student-create',
  templateUrl: './student-create.component.html',
  styleUrls: ['./student-create.component.scss']
})
export class StudentCreateComponent implements OnInit {

  constructor(
    private studentService: StudentService
  ) {
    this.newStudent();
  }

  @ViewChild('studentComponent', null) studentComponent: StudentFormComponent;

  student: Student;
  studentFormStatus = '';
  addressFormStatus = '';

  ngOnInit(): void {
  }

  newStudent(): void {
    this.student = {
      studentAddress: {}
    } as Student;
  }

  submitStudent(): void {
    if (this.isSubmiValid()) {
      this.studentService
        .createStudent(this.student)
        .toPromise()
        .then(() => {
          this.studentComponent.studentForm.reset();
          this.studentComponent.resetChildForm();
          this.firePopUp('Sucesso!', 'Salvo com sucesso.', 'success');
          this.newStudent();
        }, onRejected => {
          console.error(onRejected);
          this.firePopUp('Erro!', 'Não foi possível salvar', 'error');
        })
        .catch(error => {
          console.error(error);
          this.firePopUp('Erro!', 'Houve um erro', 'error');
        });
    } else {
      this.firePopUp('', 'Verifique os campos.', 'warning');
    }
  }

  isSubmiValid(): boolean {
    return this.studentFormStatus === 'VALID' && this.addressFormStatus === 'VALID';
  }

  setStudentFormValueFromEmition(event): void {
    this.student = event;
  }

  setStudentFormStatusFromEmition(event): void {
    this.studentFormStatus = event;
  }

  setStudentAddressValueFromEmition(event): void {
    this.student.studentAddress = event;
  }

  setAddressFormStatusFromEmition(event): void {
    this.addressFormStatus = event;
  }

  firePopUp(
    givenTitle?: string | HTMLElement | JQuery,
    givenText?: string,
    givenIcon?: SweetAlertIcon
  ): void {
    Swal.fire({
      position: 'bottom-end',
      title: givenTitle,
      text: givenText,
      icon: givenIcon,
      backdrop: true,
      timer: 1750
    });
  }
}
