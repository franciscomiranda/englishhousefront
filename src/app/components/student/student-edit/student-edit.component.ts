import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StudentService } from 'src/app/service/student.service';
import { StudentFormComponent } from '../student-form/student-form.component';
import { Student } from 'src/app/class/student';
import Swal, { SweetAlertIcon } from 'sweetalert2';

@Component({
  selector: 'app-student-edit',
  templateUrl: './student-edit.component.html',
  styleUrls: ['./student-edit.component.scss']
})
export class StudentEditComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private studentService: StudentService,
  ) {
    this.newStudent();
  }

  @ViewChild('studentComponent', null) studentComponent: StudentFormComponent;

  student: Student;
  studentId: number;
  addressId: number;
  isStudentActive: boolean;
  studentFormStatus = '';
  addressFormStatus = '';

  async ngOnInit() {
    await this.activatedRoute.params
      .subscribe(parameters => {
        this.studentId = parameters.studentId;
      });

    this.studentService
      .readStudentById(this.studentId)
      .toPromise()
      .then(studentResponse => {
        this.student = studentResponse;
        this.addressId = studentResponse.studentAddress.addressId;
        this.isStudentActive = studentResponse.studentIsActive;
      }, onRejected => {
        console.error(onRejected);
        this.firePopUp('Erro!', 'Não foi possível salvar.', 'error');
      })
      .catch(error => {
        console.error(error);
        this.firePopUp('Erro!', 'Houve um erro.', 'error');
      });
  }


  newStudent(): void {
    this.student = {
      studentAddress: {}
    } as Student;
  }

  submitStudent(): void {
    if (this.isSubmitValid()) {
      this.student.studentId = parseInt(this.studentId.valueOf().toString(), 10);
      this.student.studentAddress.addressId = parseInt(this.addressId.valueOf().toString(), 10);
      this.student.studentIsActive = this.isStudentActive;
      this.studentService.updateStudent(this.student)
        .toPromise()
        .then(() => {
          this.studentComponent.studentForm.reset();
          this.studentComponent.resetChildForm();
          this.firePopUp('Sucesso', null, 'success', () => { this.router.navigate(['/student/search']); });
        }, onRejected => {
          console.error(onRejected);
          this.firePopUp('Erro!', 'Não foi possível salvar.', 'error');
        })
        .catch(error => {
          console.error(error);
          this.firePopUp('Erro!', 'Houve um erro.', 'error');
        });
    } else {
      this.firePopUp(null, 'Verifique os campos.', 'warning');
    }
  }

  isSubmitValid(): boolean {
    return this.studentFormStatus === 'VALID' && this.addressFormStatus === 'VALID';
  }

  setStudentValueFromEmition(event): void {
    this.student = event;
  }

  setStudentFormStatusFromEmition(event): void {
    this.studentFormStatus = event;
  }

  setStudentAddressValueFromEmition(event): void {
    this.student.studentAddress = event;
  }

  setAddressFormStatusFromEmition(event): void {
    this.addressFormStatus = event;
  }

  firePopUp(
    givenTitle?: string | HTMLElement | JQuery,
    givenText?: string,
    givenIcon?: SweetAlertIcon,
    givenOnDestroy?: any
  ): void {
    Swal.fire({
      position: 'bottom-end',
      title: givenTitle,
      text: givenText,
      icon: givenIcon,
      backdrop: true,
      timer: 1750,
      onDestroy: givenOnDestroy
    });

  }
}
