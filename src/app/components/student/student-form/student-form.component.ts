import { Component, OnDestroy, OnInit, ViewChild, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AddressFormComponent } from '../../address/address-form/address-form.component';
import { DocumentType } from 'src/app/class/document-type';
import { Student } from 'src/app/class/student';
import { Subscription } from 'rxjs';
import { StudentService } from 'src/app/service/student.service';
import Swal, { SweetAlertIcon } from 'sweetalert2';
import { Address } from 'src/app/class/address';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.scss']
})
export class StudentFormComponent implements OnInit, OnChanges, OnDestroy {

  constructor(
    private formBuilder: FormBuilder,
  ) {
    this.buildForm();
    this.setDocumentTypeArray();
    this.setSubscriptions();
    this.newStudent();
  }

  @Input() studentInput: Student;
  @Input() formBehaviorName: string;
  @Output() studentEmitter = new EventEmitter();
  @Output() studentFormStatusEmitter = new EventEmitter();
  @Output() addressEmitter = new EventEmitter();
  @Output() addressFormStatusEmitter = new EventEmitter();
  @Output() submitEmitter = new EventEmitter();
  @ViewChild('addressComponent', null) addressComponent: AddressFormComponent;
  @ViewChild('documentNumber', null) documentNumber: ElementRef;
  @ViewChild('documentType', null) documentType: any;
  @ViewChild('phone', null) phone: any;

  student: Student;
  studentForm: FormGroup;
  studentAddress: Address;
  documentTypeArray = [] as DocumentType[];
  subscriptionArray = [] as Subscription[];
  documentMask: string;
  phoneMask: string;

  ngOnChanges({ studentInput: { currentValue } }: SimpleChanges) {
    if (currentValue) {
      this.setStudentFormValueFromInput(currentValue);
      this.studentAddress = currentValue.studentAddress;
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subscriptionArray
      .forEach(subscription =>
        subscription.unsubscribe);
  }

  newStudent(): void {
    this.student = {} as Student;
  }

  setStudentValue(): void {
    this.student.studentFirstName = this.studentForm.get('txtStudentFirstName').value;
    this.student.studentLastName = this.studentForm.get('txtStudentLastName').value;
    this.student.studentBirthDate = this.studentForm.get('dtStudentBirthDate').value;
    this.student.studentDocumentType = this.studentForm.get('slStudentDocumentType').value;
    this.student.studentDocumentNumber = this.studentForm.get('txtStudentDocumentNumber').value;
    this.student.studentPhone = this.studentForm.get('txtStudentPhone').value;
    this.student.studentEmail = this.studentForm.get('txtStudentEmail').value;
  }

  setStudentFormValueFromInput(studentInput): void {
    this.studentForm.get('txtStudentFirstName').setValue(studentInput.studentFirstName);
    this.studentForm.get('txtStudentLastName').setValue(studentInput.studentLastName);
    this.studentForm.get('dtStudentBirthDate').setValue(studentInput.studentBirthDate);
    this.studentForm.get('slStudentDocumentType').setValue(studentInput.studentDocumentType);
    this.studentForm.get('txtStudentDocumentNumber').setValue(studentInput.studentDocumentNumber);
    this.studentForm.get('txtStudentPhone').setValue(studentInput.studentPhone);
    this.studentForm.get('txtStudentEmail').setValue(studentInput.studentEmail);
  }

  setSubscriptions(): void {
    let auxSubscription =
      this.studentForm.valueChanges
        .subscribe(() => {
          this.setStudentValue();
          this.emitStudent();
        });
    this.subscriptionArray
      .push(auxSubscription);

    auxSubscription =
      this.studentForm.statusChanges
        .subscribe(newStatus =>
          this.emitStudentFormStatus(newStatus));
    this.subscriptionArray
      .push(auxSubscription);
  }

  setDocumentMaskFormat(): void {
    const stringType: string = this.documentType.value;
    if (stringType === 'CPF') {
      this.documentMask = '000.000.000-00';
    } else if (stringType === 'CNPJ') {
      this.documentMask = '00.000.000/0000-00';
    }
  }

  setPhoneMaskFormat(): void {
    const stringValue: string = this.phone.nativeElement.value;
    const stringLength = stringValue.length;
    if (stringLength === 13) {
      this.phoneMask = '(00)0000-00000';
    } else if (stringLength === 14) {
      this.phoneMask = '(00)00000-0000';
    }
  }

  setDocumentTypeArray(): void {
    this.documentTypeArray = [
      { documentTypeId: 1, documentTypeName: 'CPF' },
      { documentTypeId: 2, documentTypeName: 'CNPJ' }
    ];
  }

  emitStudent(): void {
    this.studentEmitter.emit(this.student);
  }

  emitStudentFormStatus(newStatus): void {
    this.studentFormStatusEmitter.emit(newStatus);
  }

  emitAddress(event): void {
    this.addressEmitter.emit(event);
  }

  emitAddressFormStatus(event): void {
    this.addressFormStatusEmitter.emit(event);
  }

  emitSubmit(): void {
    this.submitEmitter.emit();
  }


  resetChildForm(): void {
    this.addressComponent.resetForm();
  }

  buildForm(): void {
    this.studentForm = this.formBuilder.group({
      txtStudentFirstName: [null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(25)
      ])],
      txtStudentLastName: [null, Validators.maxLength(40)],
      dtStudentBirthDate: [null, []],
      slStudentDocumentType: [null, Validators.required],
      txtStudentDocumentNumber: [null, Validators.compose([
        Validators.required,
        Validators.minLength(11),
        Validators.maxLength(14)
      ])],
      txtStudentPhone: [null, Validators.compose([
        Validators.minLength(10),
        Validators.maxLength(11)
      ])],
      txtStudentEmail: [null, Validators.compose([
        Validators.email,
        Validators.maxLength(50)
      ])]
    });
    this.studentForm.get('dtStudentBirthDate').disable();
  }
}
