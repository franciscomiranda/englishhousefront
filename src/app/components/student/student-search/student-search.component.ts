import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-student-search',
  templateUrl: './student-search.component.html',
  styleUrls: ['./student-search.component.scss']
})
export class StudentSearchComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.studentSearchForm = this.getSearchForm();
  }

  studentSearchForm: FormGroup;
  nameValue = '';
  documentValue = '';
  statusValue = false;

  ngOnInit() {
  }

  setNameValue(value): void {
    this.nameValue = value.target.value;
  }
  setDocumentValue(value): void {
    this.documentValue = value.target.value;
  }
  setStatusValue(value): void {
    this.statusValue = value.checked;
  }

  getSearchForm() {
    return this.formBuilder.group({
      txtStudentName: [],
      txtStudentDocument: [],
      ckStudentStatus: []
    });
  }

}
