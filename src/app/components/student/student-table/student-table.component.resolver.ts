import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { Student } from 'src/app/class/student';
import { StudentService } from 'src/app/service/student.service';

@Injectable({
    providedIn: 'root'
})
export class StudentTableResolver implements Resolve<Observable<Student[]>> {

    constructor(private studentService: StudentService) { }

    resolve(): Observable<Student[]> {
        return this.studentService.readAllStudents();
    }
}
