import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatDialog, MatDialogRef, MatPaginator } from '@angular/material';
import { Student } from 'src/app/class/student';
import { StudentService } from 'src/app/service/student.service';
import Swal, { SweetAlertIcon } from 'sweetalert2';
import { StudentAddressDialogComponent } from '../../dialogs/student-address-dialog/student-address-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-student-table',
  templateUrl: './student-table.component.html',
  styleUrls: ['./student-table.component.scss']
})
export class StudentTableComponent implements OnInit, OnDestroy {

  constructor(
    private activatedRoute: ActivatedRoute,
    private matDialog: MatDialog,
    private studentService: StudentService
  ) { }

  @Input() nameValue: string;
  @Input() documentValue: string;
  @Input() statusValue: any;
  @ViewChild(MatPaginator, { static: true }) tablePaginator: MatPaginator;

  studentTableHasData: boolean;
  studentTableDataSource: MatTableDataSource<Student>;
  subscriptionsArray: Subscription[] = [];
  columnsToDisplay: string[] = [
    'active',
    'name',
    'birth',
    'documentType',
    'documentNumber',
    'phone',
    'email',
    'actions'];

  ngOnInit() {
    this.setStudentTableDataSourceFromResolver();
    this.tablePaginator._intl.itemsPerPageLabel = null;
    this.tablePaginator.showFirstLastButtons = true;
  }

  ngOnDestroy() {
    this.subscriptionsArray
      .forEach(subscription =>
        subscription.unsubscribe());
  }

  setStudentTableDataSource(): void {
    this.studentService
      .readAllStudents()
      .toPromise()
      .then(studentData => {
        this.studentTableDataSource = new MatTableDataSource(studentData);
        this.studentTableDataSource.paginator = this.tablePaginator;
        if (studentData.length > 0) {
          this.studentTableHasData = true;
        } else {
          this.studentTableHasData = false;
        }
      }, onRejected => {
        console.error(onRejected);
        this.firePopUp('Erro!', 'Erro ao procurar os registros.', 'error');
      }).catch(error => {
        console.error(error);
        this.firePopUp('Erro!', 'Houve um erro.', 'error');
      });
  }

  setStudentTableDataSourceFromResolver(): void {
    const studentData = this.activatedRoute.snapshot.data.resolvedStudentArray;
    this.studentTableDataSource = new MatTableDataSource(studentData);
    this.studentTableDataSource.paginator = this.tablePaginator;
    if (studentData.length > 0) {
      this.studentTableHasData = true;
    } else {
      this.studentTableHasData = false;
    }
  }

  getStudentFullName(student: Student): string {
    return student.studentLastName ?
      student.studentFirstName + ' ' + student.studentLastName :
      student.studentFirstName;
  }

  deleteStudent(student: Student): void {
    this.fireAsurePopUp('Confirma?', 'A operação não pode ser desfeita.', 'question').then(flag => {
      if (flag) {
        this.studentService.deleteStudent(student.studentId)
          .toPromise()
          .then(() => {
            this.setStudentTableDataSource();
            this.firePopUp('Sucesso!', null, 'success');
          }, onRejected => {
            console.error(onRejected);
            this.firePopUp('Erro!', 'Não foi possível excluir o registro.', 'error');
          }).catch(error => {
            console.error(error);
            this.firePopUp('Erro!', 'Houve um erro.', 'error');
          });
      }
    });
  }

  toggleStudentActiveStatus(student: Student) {
    this.studentService
      .updateStudentActiveStatus(student)
      .toPromise()
      .then(() => {
        this.setStudentTableDataSource();
        this.firePopUp('Sucesso', null, 'success', 750);
      }, onRejected => {
        console.error(onRejected);
        this.firePopUp('Erro!', 'Não foi possível realizar a operação.', 'error');
      }).catch(error => {
        console.error(error);
        this.firePopUp('Erro!', 'Houve um erro.', 'error');
      });
  }

  openStudentAddressDialog(student: Student) {
    this.matDialog.open(StudentAddressDialogComponent, {
      data: { student },
      hasBackdrop: true,
      height: '210px',
      width: '650px'
    });
  }

  async fireAsurePopUp(
    givenTitle?: string | HTMLElement | JQuery,
    givenText?: string,
    givenIcon?: SweetAlertIcon,
    givenTimer?: number) {
    const { value: resultFlag } = await Swal.fire({
      title: givenTitle,
      text: givenText,
      icon: givenIcon,
      backdrop: true,
      position: 'bottom',
      confirmButtonText: 'Sim',
      confirmButtonColor: 'primary',
      showConfirmButton: true,
      cancelButtonText: 'Não',
      cancelButtonColor: 'warn',
      showCancelButton: true,
      reverseButtons: true
    });
    return resultFlag;
  }

  firePopUp(
    givenTitle?: string | HTMLElement | JQuery,
    givenText?: string,
    givenIcon?: SweetAlertIcon,
    givenTimer?: number) {
    Swal.fire({
      title: givenTitle,
      text: givenText,
      icon: givenIcon,
      timer: givenTimer ? givenTimer : 1750,
      backdrop: true,
      position: 'bottom-end'
    });
  }
}
