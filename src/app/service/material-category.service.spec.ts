import { TestBed } from '@angular/core/testing';

import { MaterialCategoryService } from './material-category.service';

describe('MaterialCategoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MaterialCategoryService = TestBed.get(MaterialCategoryService);
    expect(service).toBeTruthy();
  });
});
