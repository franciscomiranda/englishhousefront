import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MaterialCategory } from '../class/material-category';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MaterialCategoryService {

  constructor(private httpClient: HttpClient) { }

  private endPoint = 'http://localhost:8080/materialcategory/';

  createMaterialCategory(materialCategory: MaterialCategory): Observable<MaterialCategory> {
    return this.httpClient
      .post<MaterialCategory>(this.endPoint + 'create', materialCategory)
      .pipe(catchError(this.handleError));
  }

  readAll(): Observable<MaterialCategory[]> {
    return this.httpClient
      .get<MaterialCategory[]>(this.endPoint + 'readall')
      .pipe(catchError(this.handleError));
  }

  readActives(): Observable<MaterialCategory[]> {
    return this.httpClient
      .get<MaterialCategory[]>(this.endPoint + 'readactives')
      .pipe(catchError(this.handleError));
  }

  readById(materialCategoryId: number): Observable<MaterialCategory> {
    return this.httpClient
      .get<MaterialCategory>(this.endPoint + 'readbyid/' + materialCategoryId)
      .pipe(catchError(this.handleError));
  }

  update(materialCategory: MaterialCategory): Observable<MaterialCategory> {
    return this.httpClient
      .put<MaterialCategory>(this.endPoint + 'update', materialCategory)
      .pipe(catchError(this.handleError));
  }

  updateCategoryActiveStatus(materialCategory: MaterialCategory): Observable<MaterialCategory> {
    return this.httpClient
      .put<MaterialCategory>(this.endPoint + 'updateactivestatus', materialCategory)
      .pipe(catchError(this.handleError));
  }

  deleteMaterialCategory(materialCategoryId: number): Observable<any> {
    return this.httpClient
      .delete(this.endPoint + 'delete/' + materialCategoryId)
      .pipe(catchError(this.handleError));
  }

  private handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
