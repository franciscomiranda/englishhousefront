import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { Material } from '../class/material';


@Injectable({
  providedIn: 'root'
})
export class MaterialService {

  constructor(private httpClient: HttpClient) { }

  private endPoint = 'http://localhost:8080/material/';

  postMaterial(material: Material): Observable<Material> {
    return this.httpClient
      .post<Material>(this.endPoint + 'post', material)
      .pipe(catchError(this.handleError));
  }

  getAllMaterial(): Observable<Material[]> {
    return this.httpClient
      .get<Material[]>(this.endPoint + 'get-all')
      .pipe(catchError(this.handleError));
  }

  getMaterialById(materialId: number) {

  }

  getActiveMaterial(): Observable<Material[]> {
    return this.httpClient
      .get<Material[]>(this.endPoint + 'get-actives')
      .pipe(catchError(this.handleError));
  }

  putMaterial(material: Material) {

  }

  deleteMaterial() {

  }

  deactivateMaterial() {

  }

  private handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
