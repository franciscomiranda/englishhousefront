import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { Professor } from '../class/professor';

@Injectable({
  providedIn: 'root'
})
export class ProfessorService {

  constructor(private httpClient: HttpClient) { }

  private endPoint = 'http://localhost:8080/professor/';

  createProfessor(professor: Professor): Observable<Professor> {
    return this.httpClient
      .post<Professor>(this.endPoint + 'create', professor)
      .pipe(catchError(this.handleError));
  }

  readAll(): Observable<Professor[]> {
    return this.httpClient
      .get<Professor[]>(this.endPoint + 'readall')
      .pipe(catchError(this.handleError));
  }

  readProfessorById(professorId: number): Observable<Professor> {
    return this.httpClient
      .get<Professor>(this.endPoint + 'readbyid/' + professorId)
      .pipe(catchError(this.handleError));
  }

  updateProfessor(professor: Professor): Observable<Professor> {
    return this.httpClient
      .put<Professor>(this.endPoint + 'update', professor)
      .pipe(catchError(this.handleError));
  }

  updateProfessorActiveStatus(professor: Professor): Observable<Professor> {
    return this.httpClient
      .put<Professor>(this.endPoint + 'updateactivestatus', professor)
      .pipe(catchError(this.handleError));
  }

  deleteProfessor(professorId: number): Observable<any> {
    return this.httpClient
      .delete(this.endPoint + 'delete/' + professorId)
      .pipe(catchError(this.handleError));
  }

  private handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
