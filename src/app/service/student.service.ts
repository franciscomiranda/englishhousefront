import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Student } from '../class/student';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private httpClient: HttpClient) { }

  private endPoint = 'http://localhost:8080/student/';

  createStudent(student: Student): Observable<any> {
    return this.httpClient
      .post<Student>(this.endPoint + 'create', student)
      .pipe(catchError(this.handleError));
  }

  readAllStudents(): Observable<Student[]> {
    return this.httpClient
      .get<Student[]>(this.endPoint + 'readall')
      .pipe(catchError(this.handleError));
  }

  readStudentById(studentId: number): Observable<Student> {
    return this.httpClient
      .get<Student>(this.endPoint + 'readbyid/' + studentId)
      .pipe(catchError(this.handleError));
  }

  updateStudent(student: Student): Observable<Student> {
    return this.httpClient
      .put<Student>(this.endPoint + 'update', student)
      .pipe(catchError(this.handleError));
  }

  updateStudentActiveStatus(student: Student): Observable<Student> {
    return this.httpClient
      .put<Student>(this.endPoint + 'updateactivestatus', student)
      .pipe(catchError(this.handleError));
  }

  deactivateStudent(student: Student): Observable<any> {
    return this.httpClient.put<Student>(this.endPoint + 'deactivate/', student).pipe(catchError(this.handleError));
  }

  deleteStudent(studentId: number): Observable<any> {
    return this.httpClient.delete(this.endPoint + 'delete/' + studentId).pipe(catchError(this.handleError));
  }

  private handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
